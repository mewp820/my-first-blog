from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

from .models import *

# Create your views here.
@login_required
def home(request):
	''' home view which gets all posts'''
	if request.user.is_authenticated():
		userName = request.user.id
		posts = Post.objects.filter(author=userName);
	else:
		posts = {"posts": []}
	return render(request, 'blog/home.html', {"posts": posts})


@login_required
def addPost(request):
	''' addPost lets you add one post'''
	if request.user.is_authenticated():
		userName = request.user.id
		# posts = Post.objects.filter(author=userName);
		test = {"status": 'succcess'}
	else:
		test = {"status": 'error'}
	return render(request, 'blog/addPost.html', test)





#generic view
def RegisterSuccess(request):
	
	return render(request, "registersuccess.html", {})