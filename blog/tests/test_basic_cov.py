from django.test import TestCase
from django.test.client import Client

from django.contrib.auth.models import User

from blog.models import *


class BasicUnloggedInTestCase(TestCase):

	def test_login_redirect(self):
		self.client.get('/logout')
		response = self.client.get('/')
		self.assertEqual(response.status_code, 302)
		self.assertRedirects(response, '/login/?next=/')


class BasicLoggedinTestCase(TestCase):
	def setUp(self):
		# self.client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.credentials = {
            'username': 'temporaryuser',
            'password': 'temporary123'}
		User.objects.create_user(**self.credentials)

		self.client.post('/login/', self.credentials, follow=True)


	# def test_login_home(self):
	# 	response = self.client.get('/')

	# 	self.assertTrue(response.context['user'].is_authenticated())
	# 	self.assertContains(response, '<h1>My posts</h1>')

	# def test_login_addposts(self):
	# 	# self.client.post('/login/', self.credentials, follow=True)
	# 	response = self.client.get('/addPosts')
	# 	self.assertTrue(response.context['user'].is_authenticated())
	# 	self.assertContains(response, '<h1>New Post</h1>')

class PresenceOfPostsTestCase(TestCase):
	def setUp(self):
		self.credentials = {
            'username': 'temporaryuser',
            'password': 'temporary123'}
		User.objects.create_user(**self.credentials)

		self.client.post('/login/', self.credentials, follow=True)

	def test_create_two_posts(self):
		response = self.client.get('/addPost')
		
		user_id = response.context['user'].id
		user = User.objects.get(pk=user_id)

		initial_num_of_posts = Post.objects.all().count()
		self.assertEqual(initial_num_of_posts, 0)

		Post.objects.bulk_create([
			Post(author=user, title='title1'), 
			Post(author=user, title='title2')
		])
		number_of_new_posts = Post.objects.filter(author=user_id).count()

		self.assertEqual(number_of_new_posts, 2)
 