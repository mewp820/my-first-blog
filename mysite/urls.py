"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from django.views.generic.edit import CreateView

from django.views.generic import TemplateView


from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.views import login, logout

from blog import views
urlpatterns = [

 	url(r'^register/', CreateView.as_view(
            template_name='register.html',
            form_class=UserCreationForm,
            success_url='/registersuccess/'
    )),
    url(r'^registersuccess/$', views.RegisterSuccess, name='registersuccess'),
    url(r'^login/', login , {'template_name': 'login.html', }),
    url(r'^logout/', logout,  {'next_page': views.home, }),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', views.home, name='home'),
    url(r'^addPost$', views.addPost, name='addPost'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

